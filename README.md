# Jenkins on Docker (-compose)
_A simple and fast implementation for multiple purpose_

## Before to use
Please, check if your system has git, **Docker** and **Docker-compose** installed.
For to install Docker and Docker-compose fast (and not-furious), see: https://gitlab.com/wenupix/docker-autoinstall

## Get ready...
Dowload this script:
```sh
git clone https://gitlab.com/wenupix/jenkins_master
```

Enter to folder an run docker-compose
```sh
cd jenkins_master/
docker-compose up -d --build
```

## IMPORTANT!
If yo need to stop Jenkins, is recommended shutdown from URL: [http://<JENKINS_IP_OR_HOSTNAME>:<PORT>/exit](http://<JENKINS_IP_OR_HOSTNAME>:<PORT>/exit) . Next, stop docker-compose: 
```sh
docker-compose stop
```

If Jenkins is stopped: to start again, run:
```sh
docker-compose up 
```
(... _-d --build_ parameters is not needed)


EOF
